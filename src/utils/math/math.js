export const getFibonacciSeries = (limit = 100) => {
  const fibSequence = [1, 1];
  if (limit === 1) return fibSequence;

  const pushNumber = () => {
    const fibNumber = fibSequence[fibSequence.length - 1] + fibSequence[fibSequence.length - 2];
    if (fibNumber <= limit) {
      fibSequence.push(fibNumber)
      pushNumber();
    }
  }
  pushNumber();

  return fibSequence;
}

export const isPrime = (num) => {
  for (let i = 2; i < num; i++) {
    if ((num % i) === 0) {
      return false
    };
  }
  return num > 1;
}

export const isComposite = (num) => {
  return !isPrime(num);
}
