import { getFibonacciSeries, isPrime, isComposite } from "./math";

describe("math", () => {

  it("getFibonacciSeries() should return [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]", () => {
    expect(getFibonacciSeries()).toStrictEqual([1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89])
  })

  it("getFibonacciSeries(50) should return [1, 1, 2, 3, 5, 8, 13, 21, 34]", () => {
    expect(getFibonacciSeries(50)).toStrictEqual([1, 1, 2, 3, 5, 8, 13, 21, 34])
  })

  it("isPrime(2) should return true", () => {
    expect(isPrime(2)).toBe(true)
  })

  it("isPrime(1) should return false", () => {
    expect(isPrime(1)).toBe(false)
  })

  it("isPrime(5) should return true", () => {
    expect(isPrime(5)).toBe(true)
  })

  it("isComposite(2) should return true", () => {
    expect(isComposite(2)).toBe(false)
  })

  it("isComposite(1) should return false", () => {
    expect(isComposite(1)).toBe(true)
  })

  it("isComposite(5) should return true", () => {
    expect(isComposite(5)).toBe(false)
  })

});
