import { isNumber } from "util"
import { isInRange } from "./validation";

describe("validation", () => {
  it("should return true for value 4", () => {
    expect(isNumber(4)).toBe(true);
  })

  it("should return false for value 'test'", () => {
    expect(isNumber(4)).toBe(true);
  })

  it("should return true for value 105", () => {
    expect(isNumber(4)).toBe(true);
  })

  describe("isInRange", () => {
    it("should return true for 5 in range from 1 to 10", () => {
      expect(isInRange(5, 0, 10)).toBe(true);
    })

    it("should return false for 5 in range from 6 to 10", () => {
      expect(isInRange(5, 6, 10)).toBe(false);
    })
  })
})

