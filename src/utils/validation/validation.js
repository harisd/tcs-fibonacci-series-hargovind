const toString = Object.prototype.toString;

const getTag = (value) => {
  if (value == null) {
    return value === undefined ? "[object Undefined]" : "[object Null]";
  }
  return toString.call(value);
};

export const isObjectLike = (value) =>
  typeof value === "object" && value !== null;

export const isNumber = (value) =>
  typeof value === "number" &&
  isObjectLike(value) &&
  getTag(value) === "[object Number]";

export const isInRange = (num, start, end) => {
  if (num >= start && num <= end) {
    return true;
  }
  return false;
};
