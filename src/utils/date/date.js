export const isDateWeekend = (date) => {
  const day = date.getDay();
  if (day === 0 || day === 6) {
    return true;
  }
  return false;
};

export const isTodayWeekeEnd = () => {
  return isDateWeekend(new Date());
}
