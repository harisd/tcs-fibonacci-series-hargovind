import React, { useState } from "react";
import NumberForm from "../../components/numberForm/NumberForm";
import FibonacciSeries from "../../components/fibonacciSeries/FibonacciSeries";
import { getFibonacciSeries } from "../../utils/math/math";
import { Alert } from "../../components/alert/Alert";

function FibonacciSeriesGenerator() {
  const [series, setSeries] = useState([]);

  function handleSubmit(num) {
    setSeries(getFibonacciSeries(num));
  }

  function handleClear() {
    setSeries([]);
  }

  return (
    <React.Fragment>
      <NumberForm
        max={2000}
        min={1}
        onSubmit={handleSubmit}
        onClear={handleClear}
      ></NumberForm>
      {series.length === 0 && (
        <Alert
          status="info"
          message="Once you enter max limit, Fabonacci series will be generated upto that
        that number..."
        ></Alert>
      )}
      <FibonacciSeries records={series} pageSize={10}></FibonacciSeries>
    </React.Fragment>
  );
}

export default FibonacciSeriesGenerator;
