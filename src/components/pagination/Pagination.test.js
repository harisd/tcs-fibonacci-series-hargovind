import React from "react";
import { render } from "@testing-library/react";
import { getFibonacciSeries } from "../../utils/math/math";
import { withPagination } from "./Pagination";

export const DummyComponent = ({ pageRecords }) => {
  return (
    <ul className="list-group" data-testid="list">
      {pageRecords.map((num, index) => (
        <div key={index} num={num}></div>
      ))}
    </ul>
  );
};

function renderPagination(props) {
  const defaultProps = {
    records: getFibonacciSeries(13),
    pageSize: 5,
  };
  const PaginatedComponent = withPagination(DummyComponent);
  return render(
    <PaginatedComponent {...defaultProps} {...props}></PaginatedComponent>
  );
}

describe("Pagination", () => {
  it("should display record according to size defined in pageSize property", async () => {
    const { findByTestId } = renderPagination();
    const list = await findByTestId("list");
    expect(list.children.length).toBe(5);
  });
});
