import React, { useState, useEffect } from "react";

export const withPagination = (Component) => {
  return ({ records, pageSize, noRecordLabel, ...rest }) => {
    let [pageRecords, setPageRecords] = useState([]);
    let [page, setPage] = useState(1);

    useEffect(() => {
      const from = page * pageSize - pageSize;
      const to = page * pageSize;
      setPageRecords(records.slice(from, to));
    }, [records, page, pageSize]);

    const getTotalNumberOfPages = () => Math.ceil(records.length / pageSize);

    const goToPage = (page) => {
      setPage(page);
    };

    const isLastPage = () => getTotalNumberOfPages() === page;

    const isFirstPage = () => page === 1;

    const handleNextClick = () => {
      if (isLastPage()) {
        return;
      }
      goToPage(++page);
    };

    const handlePrevClick = () => {
      if (isFirstPage()) {
        return;
      }
      goToPage(--page);
    };

    const getPages = () => {
      const pages = [];
      const totalPages = getTotalNumberOfPages();
      for (let i = 1; i <= totalPages; i++) {
        pages.push(
          <li className={`page-item ${page === i && "active"}`} key={i}>
            <button
              className="page-link"
              type="button"
              onClick={() => {
                goToPage(i);
              }}
            >
              {i}
            </button>
          </li>
        );
      }
      return pages;
    };

    const getPagination = () => (
      <React.Fragment>
        <div className="d-flex justify-content-between align-items-center">
          <nav aria-label="Page navigation example">
            <ul className="pagination mb-0">
              <li className="page-item">
                <button className="page-link" onClick={handlePrevClick}>
                  Previous
                </button>
              </li>
              {getPages()}
              <li className="page-item">
                <button className="page-link" onClick={handleNextClick}>
                  Next
                </button>
              </li>
            </ul>
          </nav>
          <div className="mr-2">
            Page {page} of {getTotalNumberOfPages()} | {pageRecords.length} out of {records.length} records
          </div>
        </div>
        <Component page={page} pageRecords={pageRecords} {...rest}></Component>
      </React.Fragment>
    );

    return (
      <React.Fragment>
        {records.length > 0 ? (
          getPagination()
        ) : (
          <div className="p-3">No Record Found...</div>
        )}
      </React.Fragment>
    );
  };
};

export default withPagination;
