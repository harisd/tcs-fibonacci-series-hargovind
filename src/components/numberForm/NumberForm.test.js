import React from "react";
import { render, fireEvent } from "@testing-library/react";
import NumberForm from "./NumberForm";

function renderNumberForm(props) {
  const defaultProps = {
    onSubmit: () => {},
    onClear: () => {},
    max: 2000,
    min: 0,
  };
  return render(<NumberForm {...defaultProps} {...props}></NumberForm>);
}

describe("NumberForm", () => {
  it("initial value of input field should be null", async () => {
    const { findByTestId } = renderNumberForm({});
    const numberForm = await findByTestId("form");
    expect(numberForm).toHaveFormValues({
      number: null,
    });
  });

  it("onSubmit should receive the input value", async () => {
    const onSubmit = jest.fn();
    const { findByTestId } = renderNumberForm({
      onSubmit,
    });
    const numberForm = await findByTestId("form");
    const inputField = await findByTestId("input-field");
    fireEvent.change(inputField, { target: { value: "33" } });
    fireEvent.submit(numberForm);
    expect(onSubmit).toHaveBeenCalledWith("33");
  });
});
