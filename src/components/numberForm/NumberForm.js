import React, { useState } from "react";
import { isInRange } from "../../utils/validation/validation";
import Errors from "../errors/Errors";
import { PropTypes } from "prop-types";

export const NumberForm = ({ max, min, onClear, onSubmit }) => {
  const [inputValue, setInputValue] = useState("");
  const [errors, setError] = useState([]);

  function validateForm() {
    const errors = [];
    if (!isInRange(inputValue, min, max)) {
      errors.push(
        `Input value must be greater then ${min} and less then ${max}`
      );
    }
    return errors;
  }

  function isValid() {
    const errors = validateForm();
    setError(errors);
    if (errors.length > 0) {
      return false;
    }
    return true;
  }

  function handleSubmit(event) {
    event.preventDefault();
    if (isValid()) {
      onSubmit(inputValue);
    }
  }

  function handleChange(event) {
    setInputValue(event.target.value);
  }

  function hancleClear() {
    setInputValue("");
    setError([]);
    onClear();
  }

  return (
    <React.Fragment>
      {errors.length > 0 && <Errors errors={errors}></Errors>}
      <form
        onSubmit={handleSubmit}
        data-testid="form"
        className="form-inline p-4 bg-light"
      >
        <label htmlFor="inputField">Enter Number</label>
        <input
          id="inputField"
          type="number"
          value={inputValue}
          onChange={handleChange}
          data-testid="input-field"
          name="number"
          className="form-control ml-3"
          placeholder="Enter number here"
        />
        <button type="submit" className="btn btn-primary ml-3">
          Generate
        </button>

        <button
          type="button"
          className="btn btn-default ml-3"
          onClick={hancleClear}
        >
          Clear
        </button>
      </form>
    </React.Fragment>
  );
};

NumberForm.prototype = {
  min: PropTypes.number,
  max: PropTypes.number,
  onSubmit: PropTypes.func,
  onClear: PropTypes.func,
};

export default NumberForm;
