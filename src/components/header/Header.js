import React from "react";
import logo from "./logo.svg";
import "./Header.scss";
import { PropTypes } from "prop-types";

export const Header = ({ logo, title }) => {
  return (
    <header className="header">
      <img src={logo} className="header__logo logo" alt={title} />
      <h3>{title}</h3>
    </header>
  );
};

Header.propTypes = {
  logo: PropTypes.string,
  title: PropTypes.string
}

Header.defaultProps = {
  logo,
  title: "Fabonacci Series Generator",
};

export default Header;
