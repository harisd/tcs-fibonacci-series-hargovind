import React from "react";
import { isPrime } from "../../utils/math/math";
import { isTodayWeekeEnd } from "../../utils/date/date";
import { PropTypes } from "prop-types";

export const FibonacciSeriesItem = ({
  num,
}) => {
  const isWeekEnd = isTodayWeekeEnd();
  const getTickToiLabel = (num) => {
    return isPrime(num) ? (
      <span className="text-primary">{isWeekEnd ? "WIC" : "TIC"}</span>
    ) : (
      <span className="text-success">{isWeekEnd ? "WOE" : "TOE"}</span>
    );
  };

  return (
    <li className="list-group-item" data-testid="list-item">
      {num === 1 ? num : getTickToiLabel(num)}
    </li>
  );
};

FibonacciSeriesItem.propTypes = {
  num: PropTypes.number
};
