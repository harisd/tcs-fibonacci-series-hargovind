import React from "react";
import { render } from "@testing-library/react";
import { FibonacciSeriesItem } from "./FibonacciSeriesItem";

function renderFibonacciSeriesItem(props) {
  const defaultProps = {
    num: 3
  };
  return render(<FibonacciSeriesItem {...defaultProps} {...props}></FibonacciSeriesItem>);
}

describe("FibonacciSeries", () => {
  it("should show TIC for number 5", async () => {
    const { findByTestId } = renderFibonacciSeriesItem({
      num: 5
    });
    const listItem = await findByTestId("list-item");
    expect(listItem.textContent).toBe("TIC");
  });

  it("should show 1 for number 1", async () => {
    const { findByTestId } = renderFibonacciSeriesItem({
      num: 1
    });
    const listItem = await findByTestId("list-item");
    expect(listItem.textContent).toBe("1");
  });

  it("should show TOE for number 8", async () => {
    const { findByTestId } = renderFibonacciSeriesItem({
      num: 8
    });
    const listItem = await findByTestId("list-item");
    expect(listItem.textContent).toBe("TOE");
  });
});
