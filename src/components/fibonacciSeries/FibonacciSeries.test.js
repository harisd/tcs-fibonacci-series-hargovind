import React from "react";
import { render } from "@testing-library/react";
import { FibonacciSeries } from "./FibonacciSeries";
import { getFibonacciSeries } from "../../utils/math/math";

function renderFabonacciSeries(props) {
  const defaultProps = {
    pageRecords: getFibonacciSeries(50),
  };
  return render(
    <FibonacciSeries {...defaultProps} {...props}></FibonacciSeries>
  );
}

describe("FibonacciSeries", () => {
  it("numbers should be reflacted in the list", async () => {
    const { findByTestId } = renderFabonacciSeries({});
    const list = await findByTestId("list");
    expect(list.children.length).toBe(9);
  });
});
