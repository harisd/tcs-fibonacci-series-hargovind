import React from "react";
import { FibonacciSeriesItem } from "../fibonacciSeriesItem/FibonacciSeriesItem";
import { withPagination } from "../pagination/Pagination";
import { PropTypes } from "prop-types";

export const FibonacciSeries = ({ pageRecords }) => {
  return (
    <React.Fragment>
      <ul className="list-group" data-testid="list">
        {pageRecords.map((num, index) => (
          <FibonacciSeriesItem key={index} num={num}></FibonacciSeriesItem>
        ))}
      </ul>
    </React.Fragment>
  );
};

FibonacciSeries.defaultProps = {
  pageRecords: [],
};

FibonacciSeries.propTypes = {
  pageRecords: PropTypes.arrayOf(PropTypes.number),
};

export default withPagination(FibonacciSeries);
