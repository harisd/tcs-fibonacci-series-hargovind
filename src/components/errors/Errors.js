import { Alert } from "../alert/Alert";
import React from "react";
import { PropTypes } from "prop-types";

export const Errors = ({ errors }) => {
  return (
    <React.Fragment>
      {errors.map((error, index) => (
        <Alert status="danger" message={error} key={index}></Alert>
      ))}
    </React.Fragment>
  );
};

Errors.prototype = {
  errors: PropTypes.arrayOf(PropTypes.string)
}

export default Errors;
