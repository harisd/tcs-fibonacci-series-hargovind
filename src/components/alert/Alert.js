import React from "react";
import { PropTypes } from "prop-types";

export const Alert = ({ status, message }) => {
  return <div className={`alert alert-${status} mb-0`}>{message}</div>;
};

Alert.propTypes = {
  status: PropTypes.oneOf(["success", "danger", "info"]),
};

export default Alert;
