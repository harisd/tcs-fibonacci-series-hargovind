import React from "react";

import "./App.scss";
import Header from "./components/header/Header";
import FibonacciSeriesGenerator from "./containers/fibonacciSeriesGenerator/FibonacciSeriesGenerator";

function App() {
  return (
    <div className="app">
      <Header></Header>
      <FibonacciSeriesGenerator></FibonacciSeriesGenerator>
    </div>
  );
}

export default App;
